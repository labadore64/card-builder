﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonSaveFile = new System.Windows.Forms.Button();
            this.buttonLoadFile = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxCards = new System.Windows.Forms.ListBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxElement = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxArch1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxArch2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAttackOverlay = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxAttack = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDefenseOverlay = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDefense = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxYearsAlive = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxBiography = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxKeywords = new System.Windows.Forms.ComboBox();
            this.listBoxKeywords = new System.Windows.Forms.ListBox();
            this.buttonKeywordDelete = new System.Windows.Forms.Button();
            this.buttonKeywordAdd = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxDesc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxEffect = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxForeground = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxBackground = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBoxBackground);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBoxForeground);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBoxEffect);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBoxDesc);
            this.groupBox1.Controls.Add(this.buttonKeywordDelete);
            this.groupBox1.Controls.Add(this.buttonKeywordAdd);
            this.groupBox1.Controls.Add(this.listBoxKeywords);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxKeywords);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBoxBiography);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBoxYearsAlive);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxDefenseOverlay);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxDefense);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxAttackOverlay);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxAttack);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxArch2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxArch1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxElement);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxType);
            this.groupBox1.Controls.Add(this.buttonDown);
            this.groupBox1.Controls.Add(this.buttonUp);
            this.groupBox1.Controls.Add(this.buttonRemove);
            this.groupBox1.Controls.Add(this.buttonAdd);
            this.groupBox1.Controls.Add(this.listBoxCards);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1093, 401);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cards";
            // 
            // buttonSaveFile
            // 
            this.buttonSaveFile.Location = new System.Drawing.Point(12, 419);
            this.buttonSaveFile.Name = "buttonSaveFile";
            this.buttonSaveFile.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveFile.TabIndex = 1;
            this.buttonSaveFile.Text = "Save File";
            this.buttonSaveFile.UseVisualStyleBackColor = true;
            this.buttonSaveFile.Click += new System.EventHandler(this.buttonSaveFile_Click);
            // 
            // buttonLoadFile
            // 
            this.buttonLoadFile.Location = new System.Drawing.Point(94, 418);
            this.buttonLoadFile.Name = "buttonLoadFile";
            this.buttonLoadFile.Size = new System.Drawing.Size(75, 23);
            this.buttonLoadFile.TabIndex = 2;
            this.buttonLoadFile.Text = "Load File";
            this.buttonLoadFile.UseVisualStyleBackColor = true;
            this.buttonLoadFile.Click += new System.EventHandler(this.buttonLoadFile_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(132, 32);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(178, 20);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.Leave += new System.EventHandler(this.textBoxName_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // listBoxCards
            // 
            this.listBoxCards.FormattingEnabled = true;
            this.listBoxCards.Location = new System.Drawing.Point(6, 19);
            this.listBoxCards.Name = "listBoxCards";
            this.listBoxCards.Size = new System.Drawing.Size(120, 316);
            this.listBoxCards.TabIndex = 2;
            this.listBoxCards.SelectedIndexChanged += new System.EventHandler(this.listBoxCards_SelectedIndexChanged);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(6, 341);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(60, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(72, 341);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(54, 23);
            this.buttonRemove.TabIndex = 4;
            this.buttonRemove.Text = "Delete";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.Location = new System.Drawing.Point(72, 370);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(54, 23);
            this.buttonDown.TabIndex = 6;
            this.buttonDown.Text = "Down";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.Location = new System.Drawing.Point(6, 370);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(60, 23);
            this.buttonUp.TabIndex = 5;
            this.buttonUp.Text = "Up";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "philosopher",
            "theory",
            "effect"});
            this.comboBoxType.Location = new System.Drawing.Point(132, 71);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(86, 21);
            this.comboBoxType.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(224, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Element:";
            // 
            // comboBoxElement
            // 
            this.comboBoxElement.FormattingEnabled = true;
            this.comboBoxElement.Items.AddRange(new object[] {
            "none",
            "spirit",
            "mind",
            "matter"});
            this.comboBoxElement.Location = new System.Drawing.Point(224, 71);
            this.comboBoxElement.Name = "comboBoxElement";
            this.comboBoxElement.Size = new System.Drawing.Size(86, 21);
            this.comboBoxElement.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(129, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Archetype1";
            // 
            // textBoxArch1
            // 
            this.textBoxArch1.Location = new System.Drawing.Point(135, 120);
            this.textBoxArch1.Name = "textBoxArch1";
            this.textBoxArch1.Size = new System.Drawing.Size(83, 20);
            this.textBoxArch1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Archetype2";
            // 
            // textBoxArch2
            // 
            this.textBoxArch2.Location = new System.Drawing.Point(224, 120);
            this.textBoxArch2.Name = "textBoxArch2";
            this.textBoxArch2.Size = new System.Drawing.Size(83, 20);
            this.textBoxArch2.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(173, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Overlay";
            // 
            // textBoxAttackOverlay
            // 
            this.textBoxAttackOverlay.Location = new System.Drawing.Point(178, 168);
            this.textBoxAttackOverlay.Name = "textBoxAttackOverlay";
            this.textBoxAttackOverlay.Size = new System.Drawing.Size(40, 20);
            this.textBoxAttackOverlay.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(129, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Attack";
            // 
            // textBoxAttack
            // 
            this.textBoxAttack.Location = new System.Drawing.Point(135, 168);
            this.textBoxAttack.Name = "textBoxAttack";
            this.textBoxAttack.Size = new System.Drawing.Size(32, 20);
            this.textBoxAttack.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(265, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Overlay";
            // 
            // textBoxDefenseOverlay
            // 
            this.textBoxDefenseOverlay.Location = new System.Drawing.Point(270, 168);
            this.textBoxDefenseOverlay.Name = "textBoxDefenseOverlay";
            this.textBoxDefenseOverlay.Size = new System.Drawing.Size(40, 20);
            this.textBoxDefenseOverlay.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(221, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Defense";
            // 
            // textBoxDefense
            // 
            this.textBoxDefense.Location = new System.Drawing.Point(227, 168);
            this.textBoxDefense.Name = "textBoxDefense";
            this.textBoxDefense.Size = new System.Drawing.Size(32, 20);
            this.textBoxDefense.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(129, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Years Alive";
            // 
            // textBoxYearsAlive
            // 
            this.textBoxYearsAlive.Location = new System.Drawing.Point(135, 215);
            this.textBoxYearsAlive.Name = "textBoxYearsAlive";
            this.textBoxYearsAlive.Size = new System.Drawing.Size(175, 20);
            this.textBoxYearsAlive.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(129, 239);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Biography";
            // 
            // textBoxBiography
            // 
            this.textBoxBiography.Location = new System.Drawing.Point(135, 255);
            this.textBoxBiography.Multiline = true;
            this.textBoxBiography.Name = "textBoxBiography";
            this.textBoxBiography.Size = new System.Drawing.Size(175, 80);
            this.textBoxBiography.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(316, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Keyword";
            // 
            // comboBoxKeywords
            // 
            this.comboBoxKeywords.FormattingEnabled = true;
            this.comboBoxKeywords.Items.AddRange(new object[] {
            "once-per-turn",
            "permadeath",
            "lockdown",
            "solo",
            "unfriendly",
            "aggressive",
            "sacrifice",
            "sickness",
            "neutral",
            "timid",
            "double",
            "stable",
            "pragmatist",
            "theorist",
            "ignore",
            "grip",
            "block",
            "determined"});
            this.comboBoxKeywords.Location = new System.Drawing.Point(316, 32);
            this.comboBoxKeywords.Name = "comboBoxKeywords";
            this.comboBoxKeywords.Size = new System.Drawing.Size(86, 21);
            this.comboBoxKeywords.TabIndex = 27;
            // 
            // listBoxKeywords
            // 
            this.listBoxKeywords.FormattingEnabled = true;
            this.listBoxKeywords.Location = new System.Drawing.Point(408, 32);
            this.listBoxKeywords.Name = "listBoxKeywords";
            this.listBoxKeywords.Size = new System.Drawing.Size(118, 82);
            this.listBoxKeywords.TabIndex = 29;
            // 
            // buttonKeywordDelete
            // 
            this.buttonKeywordDelete.Location = new System.Drawing.Point(316, 88);
            this.buttonKeywordDelete.Name = "buttonKeywordDelete";
            this.buttonKeywordDelete.Size = new System.Drawing.Size(86, 23);
            this.buttonKeywordDelete.TabIndex = 31;
            this.buttonKeywordDelete.Text = "Delete";
            this.buttonKeywordDelete.UseVisualStyleBackColor = true;
            this.buttonKeywordDelete.Click += new System.EventHandler(this.buttonKeywordDelete_Click);
            // 
            // buttonKeywordAdd
            // 
            this.buttonKeywordAdd.Location = new System.Drawing.Point(316, 59);
            this.buttonKeywordAdd.Name = "buttonKeywordAdd";
            this.buttonKeywordAdd.Size = new System.Drawing.Size(86, 23);
            this.buttonKeywordAdd.TabIndex = 30;
            this.buttonKeywordAdd.Text = "Add";
            this.buttonKeywordAdd.UseVisualStyleBackColor = true;
            this.buttonKeywordAdd.Click += new System.EventHandler(this.buttonKeywordAdd_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(313, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Card Text";
            // 
            // textBoxDesc
            // 
            this.textBoxDesc.Location = new System.Drawing.Point(319, 132);
            this.textBoxDesc.Multiline = true;
            this.textBoxDesc.Name = "textBoxDesc";
            this.textBoxDesc.Size = new System.Drawing.Size(207, 178);
            this.textBoxDesc.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(536, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Card Effect";
            // 
            // textBoxEffect
            // 
            this.textBoxEffect.Location = new System.Drawing.Point(542, 34);
            this.textBoxEffect.Multiline = true;
            this.textBoxEffect.Name = "textBoxEffect";
            this.textBoxEffect.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxEffect.Size = new System.Drawing.Size(535, 359);
            this.textBoxEffect.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(316, 313);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Foreground Sprite";
            // 
            // textBoxForeground
            // 
            this.textBoxForeground.Location = new System.Drawing.Point(321, 329);
            this.textBoxForeground.Name = "textBoxForeground";
            this.textBoxForeground.Size = new System.Drawing.Size(205, 20);
            this.textBoxForeground.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(315, 352);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Backgound Sprite";
            // 
            // textBoxBackground
            // 
            this.textBoxBackground.Location = new System.Drawing.Point(321, 368);
            this.textBoxBackground.Name = "textBoxBackground";
            this.textBoxBackground.Size = new System.Drawing.Size(205, 20);
            this.textBoxBackground.TabIndex = 38;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "txt files|*.txt";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "txt files|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 450);
            this.Controls.Add(this.buttonLoadFile);
            this.Controls.Add(this.buttonSaveFile);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Card Creator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxEffect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxDesc;
        private System.Windows.Forms.Button buttonKeywordDelete;
        private System.Windows.Forms.Button buttonKeywordAdd;
        private System.Windows.Forms.ListBox listBoxKeywords;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxKeywords;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxBiography;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxYearsAlive;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDefenseOverlay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDefense;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAttackOverlay;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxAttack;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxArch2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxArch1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxElement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.ListBox listBoxCards;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonSaveFile;
        private System.Windows.Forms.Button buttonLoadFile;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxBackground;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxForeground;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

