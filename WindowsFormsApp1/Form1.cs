﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BindCard();
        }

        private void listboxAdd<T>(ListBox listbox, BindingList<T> list, T newobj)
        {
            if (listbox.SelectedIndex >= 0)
            {
                if (listbox.SelectedIndex == list.Count - 1)
                {
                    list.Add(newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
                else
                {
                    list.Insert(listbox.SelectedIndex + 1, newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
            }
            else
            {
                list.Add(newobj);
            }
        }

        private void listboxRemove<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.Items.Count > 1)
            {
                if (listbox.SelectedIndex >= 0)
                {
                    list.RemoveAt(listbox.SelectedIndex);
                    if (listbox.SelectedIndex > 0 && listbox.SelectedIndex != list.Count - 1)
                    {
                        listbox.SelectedIndex--;
                    }
                }
            }
        }

        private void listboxMoveUp<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex > 0)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex - 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex - 1] = characters[0];
                listbox.SelectedIndex--;
            }
        }

        private void listboxMoveDown<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex < list.Count - 1)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex + 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex + 1] = characters[0];
                listbox.SelectedIndex++;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxCards, CardData, new Card());
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxCards, CardData);
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxCards, CardData);
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxCards, CardData);
        }

        private void listBoxCards_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetBindingKeyword();
        }

        private void buttonKeywordAdd_Click(object sender, EventArgs e)
        {
            if (comboBoxKeywords.SelectedIndex > -1) {
                string add = (string)comboBoxKeywords.Items[comboBoxKeywords.SelectedIndex];
                Card card = (Card)listBoxCards.Items[listBoxCards.SelectedIndex];
                if(card != null)
                {
                    if (!card.Keywords.Contains(add))
                    {
                        card.Keywords.Add(add);
                    }
                }
            }
        }

        private void buttonKeywordDelete_Click(object sender, EventArgs e)
        {
            if (listBoxKeywords.SelectedIndex > -1)
            {
                string selected = (string)listBoxKeywords.Items[listBoxKeywords.SelectedIndex];
                Card card = (Card)listBoxCards.Items[listBoxCards.SelectedIndex];
                if (card != null)
                {
                    if (card.Keywords.Contains(selected))
                    {
                        card.Keywords.Remove(selected);
                    }
                }
            }
        }

        private void textBoxName_Leave(object sender, EventArgs e)
        {
            CardBinding.ResetBindings(false);
        }

        private void buttonLoadFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                CardData.Clear();

                string line;
                string build = "";
                StreamReader file = new StreamReader(openFileDialog1.FileName);

                while ((line = file.ReadLine()) != null)
                {
                    build += line + Environment.NewLine;
                    if (line.StartsWith("====="))
                    {
                        CardData.Add(new Card(build));
                        build = "";
                    }
                }

                file.Close();
            }
        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter file = new StreamWriter(saveFileDialog1.FileName);
                for(int i = 0; i < CardData.Count; i++)
                {
                    file.Write(CardData[i].ProduceString());
                }

                file.Close();
            }
        }
    }
}
