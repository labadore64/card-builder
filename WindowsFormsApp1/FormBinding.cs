﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        #region Properties
        public static BindingList<Card> CardData { get; set; } = new BindingList<Card>();
        public static BindingSource CardBinding;
        public static BindingSource KeywordBinding;
        #endregion

        #region Bindings

        private void BindCard()
        {
            CardData.Add(new Card());

            CardBinding = new BindingSource(CardData, null);

            listBoxCards.DataSource = CardBinding;

            SetBindingKeyword();

            textBoxName.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDesc.DataBindings.Add(
            new Binding("Text", CardBinding,
                "CardText", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEffect.DataBindings.Add(
            new Binding("Text", CardBinding,
                "CardEffect", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxArch1.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Arch1", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxArch2.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Arch2", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxYearsAlive.DataBindings.Add(
            new Binding("Text", CardBinding,
                "YearsAlive", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBiography.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Biography", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxForeground.DataBindings.Add(
            new Binding("Text", CardBinding,
                "FgSprite", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBackground.DataBindings.Add(
            new Binding("Text", CardBinding,
                "BgSprite", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxAttack.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Attack", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDefense.DataBindings.Add(
            new Binding("Text", CardBinding,
                "Defense", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxAttackOverlay.DataBindings.Add(
            new Binding("Text", CardBinding,
                "AttackOverlay", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDefenseOverlay.DataBindings.Add(
            new Binding("Text", CardBinding,
                "DefenseOverlay", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxType.DataBindings.Add(
            new Binding("SelectedItem", CardBinding,
                "Type", false, DataSourceUpdateMode.OnPropertyChanged));
            
            comboBoxElement.DataBindings.Add(
            new Binding("SelectedItem", CardBinding,
                "Element", false, DataSourceUpdateMode.OnPropertyChanged));
            


        }

        private void SetBindingKeyword()
        {
            if (listBoxCards.SelectedIndex > -1)
            {
                Card card = (Card)listBoxCards.Items[listBoxCards.SelectedIndex];
                if (card != null)
                {
                    KeywordBinding = new BindingSource(card.Keywords, null);

                    listBoxKeywords.DataSource = KeywordBinding;
                }
            }
        }
        #endregion

    }
}
