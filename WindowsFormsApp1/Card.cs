﻿using System;
using System.ComponentModel;

namespace WindowsFormsApp1
{
    public class Card : INotifyPropertyChanged
    {

        public string Name { get; set; } = "name";
        public string Type { get; set; } = "philosopher";
        public string Element { get; set; } = "none";
        public string Arch1 { get; set; } = "";
        public string Arch2 { get; set; } = "";
        public int Attack { get; set;} 
        public int Defense { get; set; }
        public int AttackOverlay { get; set; }
        public int DefenseOverlay { get; set; }
        public string YearsAlive { get; set; } = "";
        public string Biography { get; set; } = "";

        public BindingList<string> Keywords { get; set; } = new BindingList<string>();
        public string CardText { get; set; } = "";
        public string CardEffect { get; set; } = "";
        public string FgSprite { get; set; } = "";
        public string BgSprite { get; set; } = "";

        private string textBuff;

        public event PropertyChangedEventHandler PropertyChanged;

        public Card()
        {

        }

        public Card(string data)
        {
            LoadFromString(data);
        }

        public void LoadFromString(string text)
        {
            textBuff = text;
            int len = ("[CODE START]" + Environment.NewLine).Length;
            CardEffect = text.Substring(text.IndexOf("[CODE START]")+ len, (text.IndexOf("==============") - len - (text.IndexOf("[CODE START]")))).Trim();
            Name = ReadLine();
            Type = ReadLine();
            Element = ReadLine();
            FgSprite = ReadLine();
            BgSprite = ReadLine(); // uncomment later
            Arch1 = ReadLine();
            Arch2 = ReadLine();
            Attack = int.Parse(ReadLine());
            Defense = int.Parse(ReadLine());
            AttackOverlay = int.Parse(ReadLine());
            DefenseOverlay = int.Parse(ReadLine());
            Biography = ReadLine();
            YearsAlive = ReadLine();
            Keywords = SetKeywords(ReadLine());
            CardText = ReadLine().Replace("\\n",Environment.NewLine);
        }

        private string ReadLine()
        {
            string returner = "";
            if (textBuff.IndexOf(Environment.NewLine) > -1)
            {
                returner = textBuff.Substring(0, textBuff.IndexOf(Environment.NewLine));
                textBuff = textBuff.Substring(textBuff.IndexOf(Environment.NewLine)+(Environment.NewLine).Length, textBuff.Length - textBuff.IndexOf(Environment.NewLine)- (Environment.NewLine).Length);
            }
            return returner;
        }

        public string ProduceString()
        {
            return Name + Environment.NewLine +
                Type + Environment.NewLine +
                Element + Environment.NewLine +
                FgSprite + Environment.NewLine +
                BgSprite + Environment.NewLine +
                Arch1 + Environment.NewLine +
                Arch2 + Environment.NewLine +
                Attack + Environment.NewLine +
                Defense + Environment.NewLine +
                AttackOverlay + Environment.NewLine +
                DefenseOverlay + Environment.NewLine +
                Biography + Environment.NewLine +
                YearsAlive + Environment.NewLine +
                GetKeywords() + Environment.NewLine +
                CardText.Replace(Environment.NewLine, "\\n") + Environment.NewLine +
                "[CODE START]" + Environment.NewLine +
                CardEffect.Trim() + Environment.NewLine +
                "==============" + Environment.NewLine;
        }

        private string GetKeywords()
        {
            string stringer = "";
            for(int i = 0; i < Keywords.Count; i++)
            {
                stringer += Keywords[i] + ",";
            }
            return stringer;
        }

        private BindingList<string> SetKeywords(string words)
        {
            string[] word = words.Split(',');
            BindingList<string> list = new BindingList<string>();
            for(int i = 0; i < word.Length; i++)
            {
                if (word[i].Trim() != "")
                {
                    list.Add(word[i]);
                }
            }
            return list;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
